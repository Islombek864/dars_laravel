<html>
<head>
    <title>Country</title>
    <link href="{{ asset('public/bootstrap-5/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/bootstrap-5/css/main.css') }}" rel="stylesheet">
</head>
<body>

<div>
    @yield('main')
</div>





<script src="{{ asset('public/bootstrap-5/js/bootstrap.bundle.js') }}" ></script>
<script src="{{ asset('public/bootstrap-5/js/bootstrap.js') }}" ></script>
</body>
</html>
