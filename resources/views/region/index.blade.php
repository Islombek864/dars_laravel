@extends('layouts.index')


@section('main')
    <div class="container">
        <h1>
            <a href="{{ route('indexCountry') }}" class="text-decoration-none">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-left-circle" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                </svg>
            </a>

            {{ $country->name }} Region list
        </h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Area</th>
                <th>IMG</th>
                <th>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Create
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Region create</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form action="{{ route('region.store')}}" method="post">
                                    @csrf
                                    @method('post')
                                    <div class="modal-body">
                                        <div class="mb-3">
                                            <label for="exampleInputEmail1" class="form-label">Region</label>
                                            <input type="text" class="form-control" name="name">
                                            <label for="exampleInputEmail1" class="form-label">Area</label>
                                            <input type="number" class="form-control" name="area">

                                            <input type="hidden" name="country_id" value="{{ $country->id }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary" value="Save changes">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($regions as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->area }}</td>
                <td>
                    <img src="{{ asset('public/storage/images/'.$item->img)  }}" alt="{{ $item->img }}" width="100px">
                </td>
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $item->id }}">
                        Edit
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Region Edit</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form action="{{ route('region.update',$item->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="modal-body">
                                        <div class="mb-3">
                                            <label for="exampleInputEmail1" class="form-label">Region</label>
                                            <input type="text" class="form-control" name="name" value="{{ $item->name }}">
                                            <label for="exampleInputEmail1" class="form-label">Area</label>
                                            <input type="number" class="form-control" name="area" value="{{ $item->area }}">
                                            <label for="exampleInputEmail1" class="form-label">Image</label>
                                            <input type="file" class="form-control" name="img" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary" value="Save changes">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModaldel{{ $item->id }}">
                        Delete
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModaldel{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Region delete</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mb-3">

                                        <h3>Are you sure?</h3>
                                    </div>
                                </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <form action="{{ route('region.destroy',$item->id) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button class="btn btn-danger" type="submit">Delete</button>

                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>

                </td>

            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

