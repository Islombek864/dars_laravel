<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('davlat', function (){
    try {

//        $davlat = DB::select('
//            select r.*, c.name as country from regions r join countries c on c.id = r.country_id
//        ');

        $davlat = DB::table('regions as r')
        ->join('countries as c','c.id','=','r.country_id')
        ->select('r.*','c.name as country')->get();

        return response()->json([
            'status'=> true,
            'data'=>$davlat,
            'message'=>'ok boldi'
        ]);
    }catch (Exception $exception){

        return response()->json([
            'status'=> false,
            'data'=>[],
            'message'=>$exception
        ]);
    }

});


Route::post('yarat', function (Request $request){

    try {
        $request->validate([
            'name'=>'required'
        ]);

        \App\Models\Country::create([
            'name'=>$request->name
        ]);

        return response()->json([
            'status'=> true,
            'data'=>true,
            'message'=>'yaratildi'
        ]);
    }catch (Exception $exception){

        return response()->json([
            'status'=> false,
            'data'=>false,
            'message'=>$exception->getMessage()
        ]);
    }

});


