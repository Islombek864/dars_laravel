<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');

});


Route::get('country',[\App\Http\Controllers\CountryController::class,'index'])->name('indexCountry');
Route::get('country/{id}',[\App\Http\Controllers\CountryController::class,'destroy'])->name('deleteCountry');
Route::post('country-create',[\App\Http\Controllers\CountryController::class,'store'])->name('createCountry');
Route::post('country-edit',[\App\Http\Controllers\CountryController::class,'update'])->name('editCountry');




Route::resource('region', \App\Http\Controllers\RegionController::class);




Route::get('join' , function (){

//    $join = \Illuminate\Support\Facades\DB::select('
//        select r.*,c.name as country from countries c join regions r on r.country_id = c.id limit 2
//    ');

    $join = \Illuminate\Support\Facades\DB::table('regions as r')
                        ->join('countries as c','c.id','=','r.country_id')
                        ->select('r.*','c.name as country')->paginate(2);


    dd($join);


});






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
