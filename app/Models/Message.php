<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Message extends Model
{

    public static function message($num, $message){
        $url = 'https://sms.sysdc.ru/index.php';
        $res = Http::withToken(\Illuminate\Support\Env::get('SMS_TOKEN'))
            ->attach('mobile_phone', $num)
            ->attach('message', $message)
            ->post($url);

        return $res->body();
    }
}
