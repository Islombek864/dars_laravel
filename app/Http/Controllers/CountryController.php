<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Message;
use GuzzleHttp\Client;
use http\Env;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Nexmo\Laravel\Facade\Nexmo;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


//        $hash = Hash::make('1122');
//        $pass = '1122';
//
//        if (Hash::check($pass, $hash)){
//            dd('teng');
//        }else{
//            dd('emas');
//        }


//        $url = 'https://sms.sysdc.ru/index.php';
//        $res = Http::withToken(\Illuminate\Support\Env::get('SMS_TOKEN'))
//            ->attach('mobile_phone','998911157709')
//            ->attach('message','Salom')
//            ->post($url);

//        $url = 'myurl';
//        $user = 'ali';
//        $pass = '123';
//
//        Http::withBasicAuth($user,$pass)
//            ->post($url,[
//                'number'=>'998911157709',
//                'message'=>'Salom'
//            ]);



//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://sms.sysdc.ru/index.php',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => array(
//                'mobile_phone' => '998991479684',
//                'message' => 'Salom Xabibullo'),
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Bearer Token'
//            ),
//        ));
//        $response = curl_exec($curl);
//
//        curl_close($curl);


//        dd($res);


        Message::message('998911157709','Model');









        $countries = Country::all();

        return view('country.index',compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country'=>'required'
        ]);

//        $country = Country::create([
//            'name'=>$request->country
//        ]);

        $country = new Country();
        $country->name = $request->country;
        $country->save();


        return redirect()->route('indexCountry');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $country = Country::find($request->id);
        $country->name = $request->country;
        $country->update();


        return redirect()->route('indexCountry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();

        return redirect()->route('indexCountry');

    }
}
