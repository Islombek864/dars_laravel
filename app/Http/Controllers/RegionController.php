<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'area'=>'required',
            'country_id'=>'required',
        ]);

        $region = new Region();
        $region->name = $request->name;
        $region->area = $request->area;
        $region->country_id = $request->country_id;
        $region->save();

        return redirect()->route('region.show',$request->country_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);
        $regions = Region::where('country_id','=',$id)->get();

        return view('region.index', compact('regions','country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'area'=>'required',
            'img'=>'required',
        ]);

        $imageName = rand(1,1000).time().'.'.$request->file('img')->getClientOriginalExtension();

        $request->file('img')->storeAs('public/images/', $imageName);


        $region = Region::find($id);
        $region->name = $request->name;
        $region->area = $request->area;
        $region->img = $imageName;
        $region->update();

        return redirect()->route('region.show',$region->country_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Region::find($id);
        $c_id = $region->country_id;
        if (is_file(storage_path('app/public/images/'.$region->img))){
            unlink(storage_path('app/public/images/'.$region->img));
        }
        $region->delete();

        return redirect()->route('region.show',$c_id);


    }
}
